# Pipeline for Terraform modules. This pipeline will run `terraform fmt`, `terraform plan` and `terraform apply`.
#
# ## Flowchart `apply_mode=manual` or `apply_mode=auto-approve`
# 
# ```mermaid
# flowchart TD
# 
# classDef optional stroke-dasharray: 5 5
# 
# job:terraform-plan["terraform-plan"]
# job:terraform-fmt{{"terraform-fmt"}}
# job:terraform-apply["terraform-apply"]
# 
# subgraph stage:build["stage: build"]
# job:terraform-plan
# end
# 
# subgraph stage:test["stage: test"]
# job:terraform-fmt
# end
# 
# subgraph stage:deploy["stage: deploy"]
# job:terraform-apply
# end
# 
# stage:build --> stage:test
# stage:test --> stage:deploy
# ```
# 
# ## Flowchart `apply_mode=auto-approve-non-destructive`
# 
# ```mermaid
# flowchart TD
# 
# classDef optional stroke-dasharray: 5 5
# 
# job:terraform-plan["terraform-plan"]
# job:terraform-fmt{{"terraform-fmt"}}
# job:terraform-apply:generate["terraform-apply:generate"]
# job:terraform-apply:trigger["terraform-apply:trigger"]
# 
# subgraph stage:build["stage: build"]
# job:terraform-plan
# end
# 
# subgraph stage:test["stage: test"]
# job:terraform-fmt
# end
# 
# subgraph stage:deploy["stage: deploy"]
# job:terraform-apply:generate
# job:terraform-apply:trigger
# 
# job:terraform-apply:generate --> job:terraform-apply:trigger
# end
# 
# stage:build --> stage:test
# stage:test --> stage:deploy
# 
# subgraph child:terraform-apply["child pipeline: terraform-apply"]
# child:terraform-apply:terraform-apply["terraform-apply"]
# child:terraform-apply:gitlab-issue-361574["gitlab-issue-361574"]
# end
# 
# job:terraform-apply:trigger -.- child:terraform-apply
# ```
spec:
  inputs:
    name:
      default: "terraform"
      description: The name to prefix all job names with
    dir:
      default: "."
      description: the directory to build
    terraform_version:
      default: "1.8"
      description: The version of Terraform to use
    terraform_init_args:
      default: "-lockfile=readonly"
      description: additional arguments to pass to the `terraform init` command
    apply_mode:
      default: "manual"
      description: The mode to use when applying changes.
      options:
        - manual # Requires manual approval before applying changes.
        - auto-approve # Automatically applies changes without requiring approval.
        - auto-approve-non-destructive # Automatically applies changes without requiring approval, but only if the plan is non-destructive.
---
include:
  - local: templates/terraform-fmt.yml
    inputs:
      dir: $[[ inputs.dir ]]
      name: $[[ inputs.name ]]:terraform-fmt
      version: $[[ inputs.terraform_version ]]
  - local: templates/terraform-plan.yml
    inputs:
      dir: $[[ inputs.dir ]]
      name: $[[ inputs.name ]]:terraform-plan
      init_args: $[[ inputs.terraform_init_args ]]
      version: $[[ inputs.terraform_version ]]
  - local: templates/pipeline/apply_mode/$[[ inputs.apply_mode ]].yml
    inputs:
      dir: $[[ inputs.dir ]]
      name: $[[ inputs.name ]]
      terraform_init_args: $[[ inputs.terraform_init_args ]]
      terraform_version: $[[ inputs.terraform_version ]]

$[[ inputs.name ]]:terraform-fmt:
  needs: []
