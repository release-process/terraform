# GitLab CI/CD Component repository for Terraform

This repository contains [CI/CD Components] for Terraform. Part of the [Release Process] project.

* [Documentation](https://release-process.gitlab.io/components/terraform/)

[Release Process]: https://release-process.gitlab.io/
[CI/CD Components]: https://docs.gitlab.com/ee/ci/components/

:warning: **This component repository is under active development and is not yet ready for production use. The API of individual components (inputs, jobs) might change without notice between releases (`0.x.y` versions - see [semver.org](https://semver.org/spec/v2.0.0.html))** :warning:

[TOC]

## Component pipeline (Pipeline)

Pipeline for Terraform modules. This pipeline will run `terraform fmt`, `terraform plan` and `terraform apply`.

### Flowchart `apply_mode=manual` or `apply_mode=auto-approve`

```mermaid
flowchart TD

classDef optional stroke-dasharray: 5 5

job:terraform-plan["terraform-plan"]
job:terraform-fmt{{"terraform-fmt"}}
job:terraform-apply["terraform-apply"]

subgraph stage:build["stage: build"]
job:terraform-plan
end

subgraph stage:test["stage: test"]
job:terraform-fmt
end

subgraph stage:deploy["stage: deploy"]
job:terraform-apply
end

stage:build --> stage:test
stage:test --> stage:deploy
```

### Flowchart `apply_mode=auto-approve-non-destructive`

```mermaid
flowchart TD

classDef optional stroke-dasharray: 5 5

job:terraform-plan["terraform-plan"]
job:terraform-fmt{{"terraform-fmt"}}
job:terraform-apply:generate["terraform-apply:generate"]
job:terraform-apply:trigger["terraform-apply:trigger"]

subgraph stage:build["stage: build"]
job:terraform-plan
end

subgraph stage:test["stage: test"]
job:terraform-fmt
end

subgraph stage:deploy["stage: deploy"]
job:terraform-apply:generate
job:terraform-apply:trigger

job:terraform-apply:generate --> job:terraform-apply:trigger
end

stage:build --> stage:test
stage:test --> stage:deploy

subgraph child:terraform-apply["child pipeline: terraform-apply"]
child:terraform-apply:terraform-apply["terraform-apply"]
child:terraform-apply:gitlab-issue-361574["gitlab-issue-361574"]
end

job:terraform-apply:trigger -.- child:terraform-apply
```

### Usage

```yaml
include:
  - component: gitlab.com/release-process/terraform/pipeline@0.1.0
```

### Inputs

|Name|Description|Default|
|---|---|---|
| **apply_mode** | The mode to use when applying changes. | `manual` |
| **dir** | the directory to build | `.` |
| **name** | The name to prefix all job names with | `terraform` |
| **terraform_init_args** | additional arguments to pass to the `terraform init` command | `-lockfile=readonly` |
| **terraform_version** | The version of Terraform to use | `1.8` |

## Component terraform-apply (Job)

run `terraform apply` on the specified directory

### Usage

```yaml
include:
  - component: gitlab.com/release-process/terraform/terraform-apply@0.1.0
```

### Inputs

|Name|Description|Default|
|---|---|---|
| **args** | additional arguments to pass to the `terraform apply` command | `plan.tfplan` |
| **dir** | the directory to build | `.` |
| **init_args** | additional arguments to pass to the `terraform init` command | `-lockfile=readonly` |
| **name** | The name of the job application | `terraform:terraform-apply` |
| **stage** | stage of the job | `deploy` |
| **version** | terraform version to use for building | `1.8` |

## Component terraform-fmt (Job)

run `terraform fmt` on the specified directory

### Usage

```yaml
include:
  - component: gitlab.com/release-process/terraform/terraform-fmt@0.1.0
```

### Inputs

|Name|Description|Default|
|---|---|---|
| **args** | additional arguments to pass to the `terraform fmt` command | `-check -diff` |
| **dir** | the directory to build | `.` |
| **name** | The name of the job application | `terraform:terraform-fmt` |
| **stage** | stage of the job | `test` |
| **version** | terraform version to use for building | `1.8` |

## Component terraform-plan (Job)

run `terraform plan` on the specified directory and save the output plan.tfplan and plan.json

### Usage

```yaml
include:
  - component: gitlab.com/release-process/terraform/terraform-plan@0.1.0
```

### Inputs

|Name|Description|Default|
|---|---|---|
| **args** | additional arguments to pass to the `terraform plan` command | `` |
| **dir** | the directory to build | `.` |
| **init_args** | additional arguments to pass to the `terraform init` command | `-lockfile=readonly` |
| **name** | The name of the job application | `terraform:terraform-plan` |
| **stage** | stage of the job | `build` |
| **version** | terraform version to use for building | `1.8` |

### Artifacts

- `$[[ inputs.dir ]]/plan.tfplan`
- `$[[ inputs.dir ]]/plan.json`

**Expires in:** `1 week`.

